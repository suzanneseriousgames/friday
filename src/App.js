import React, {Component} from 'react';
import tt from './Images/tt.jpg'
import './App.css';

import Intro from './Components/Intro'
import Buttons from './Components/Buttons'

//TODO
//if press friday before any hours this week alert get to work
//if press friday when more than 24 hours left alert it aint friday yet!
//turn days hours into an array
//press button each day to start & stop recording time

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      appTotalHours: 36.5
    }
    this.onTotalHoursChanged = this.onTotalHoursChanged.bind(this)
  }

  onTotalHoursChanged(e) {
    this.setState({appTotalHours: e.target.value})
  }

  render() {
    return (
      <div className="App">
          <img src={tt} className="App-logo" alt="logo" />
           <Intro />
           <p>Total hours you need to work this week: 
              <input type="number" placeholder={this.state.appTotalHours} onChange={this.onTotalHoursChanged} />
            </p>
            <p>Add your hours worked each day<br />and we'll calculate what time you can leave on Friday :D</p>
           <Buttons totalHours={this.state.appTotalHours} />
           <p className="App-footer">Made by Suzanne, with React.js Dec 2019</p>
      </div>
    )
  }
}

export default App;
