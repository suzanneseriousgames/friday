import React, {Component} from 'react'
import PropTypes from 'prop-types'
import Days from './Days'
import Friday from './Friday'
import HoursToGo from './HoursToGo'
import {ConvertToTime} from './Helper'

class Buttons extends Component {

    constructor(props) {
        super(props);
        const storedMondayHours = localStorage.getItem('storedMondayHours') ? localStorage.getItem('storedMondayHours') : 0
        const storedTuedayHours = localStorage.getItem('storedTuedayHours') ? localStorage.getItem('storedTuedayHours') : 0
        const storedWednesdayHours = localStorage.getItem('storedWednesdayHours') ? localStorage.getItem('storedWednesdayHours') : 0
        const storedThursdayHours = localStorage.getItem('storedThursdayHours') ? localStorage.getItem('storedThursdayHours') : 0

        this.state = {
          showMonToThur: false,
          dayToDisplay: null,
          mondayHours: storedMondayHours,
          tuesdayHours: storedTuedayHours,
          wednesdayHours: storedWednesdayHours,
          thursdayHours: storedThursdayHours
        };
        this.handleDayClick = this.handleDayClick.bind(this)
        this.dayUpdatesHours = this.dayUpdatesHours.bind(this)
      }

    handleDayClick(e) {
        if (e.target.id !== "Friday") {
            this.setState({
                showDay: true,
                dayToDisplay: e.target.id
            });
        }else{
            this.setState({
                showDay: false,
                dayToDisplay: e.target.id
            });
        }
        //e.preventDefault()//for links
        console.log('clicked: ' + e.target.id)
    }

    dayUpdatesHours(day, hours) {
        //called by Days on time added and hours submitted
        //ref to this func set in render as Days' updateHours prop
        console.log("update - day: " + day + " hours: " + hours)
        if (day === "Monday") {
            this.setState({mondayHours: hours})
            localStorage.setItem('storedMondayHours', hours)//store in local storage
        }
        if (day === "Tuesday") {
            this.setState({tuesdayHours: hours})
            localStorage.setItem('storedTuedayHours', hours)//store in local storage
        }
        if (day === "Wednesday") {
            this.setState({wednesdayHours: hours})
            localStorage.setItem('storedWednesdayHours', hours)//store in local storage
        }
        if (day === "Thursday") {
            this.setState({thursdayHours: hours})
            localStorage.setItem('storedThursdayHours', hours)//store in local storage
        }
    }
    
    render() {
        const anyHours = Number(this.state.mondayHours) + Number(this.state.tuesdayHours) + Number(this.state.wednesdayHours) + Number(this.state.thursdayHours)
        let showFriday = false
        const hoursLeft = this.props.totalHours - anyHours
        if (this.state.dayToDisplay === "Friday" && anyHours > 0) {
            showFriday = true
            console.log("show friday!!!")
        }
        return (
            <div>
                <div className="Buttons-wrapper">
                    <div className="Buttons-nav"><button className="Buttons-button" id="Monday" onClick={this.handleDayClick}>Monday</button><p>{ConvertToTime(this.state.mondayHours)}</p></div>
                    <div className="Buttons-nav"><button className="Buttons-button" id="Tuesday" onClick={this.handleDayClick}>Tuesday</button><p>{ConvertToTime(this.state.tuesdayHours)}</p></div>
                     <div className="Buttons-nav"><button className="Buttons-button" id="Wednesday" onClick={this.handleDayClick}>Wednesday</button><p>{ConvertToTime(this.state.wednesdayHours)}</p></div>
                     <div className="Buttons-nav"><button className="Buttons-button" id="Thursday" onClick={this.handleDayClick}>Thursday</button><p>{ConvertToTime(this.state.thursdayHours)}</p></div>
                    <div className="Buttons-nav"><button className="Buttons-button" id="Friday" onClick={this.handleDayClick}>Friday</button><p>???</p></div>
                </div>
                <HoursToGo hours={hoursLeft} />
                {this.state.showDay ? <Days initialMondayHours={this.state.mondayHours} initialTuesdayHours={this.state.tuesdayHours} initialWednesdayHours={this.state.wednesdayHours} initialThursdayHours={this.state.thursdayHours} updateHours={this.dayUpdatesHours} day={this.state.dayToDisplay} /> : null }
                {showFriday ? <Friday hoursToWork={hoursLeft} /> : null}
            </div>
        );
      }
}

Buttons.propType = {
    totalHours: PropTypes.number.isRequired
}

export default Buttons