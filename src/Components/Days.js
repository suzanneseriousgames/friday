import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {ConvertToTime} from './Helper'

class Days extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mondayHours: this.props.initialMondayHours,
            tuesdayHours: this.props.initialTuesdayHours,
            wednesdayHours: this.props.initialWednesdayHours,
            thursdayHours: this.props.initialThursdayHours
          }
          this.handleTimeAdded = this.handleTimeAdded.bind(this)
          this.handleHoursSubmitted = this.handleHoursSubmitted.bind(this)
    }

    timeStringToFloat(time) {
        console.log("converting lunch: " + time)
        const hoursMinutes = time.split(/[.:]/);
        const hours = parseInt(hoursMinutes[0], 10);
        const minutes = hoursMinutes[1] ? parseInt(hoursMinutes[1], 10) : 0;
        const timeAsDecimal = hours + minutes / 60
        console.log("converted lunch: " + timeAsDecimal)
        return timeAsDecimal
      }

    handleHoursSubmitted(e) {
        e.preventDefault()//stop the browser refreshing

        console.log("handle hours submitted")
        console.log(e.target.startTime.value)
        console.log(e.target.endTime.value)
        console.log(e.target.lunchTime.value)
        
        const startTime = e.target.startTime.value
        const endTime = e.target.endTime.value
        const lunchTime = this.timeStringToFloat(e.target.lunchTime.value)

        console.log("adding hours to day: " + e.target.id)
        
        const startDate = new Date("01/01/2019 " + startTime)
        const endDate = new Date("01/01/2019 " + endTime)
        const difference = endDate - startDate
        const differenceHours = difference / 60 / 60 / 1000
        console.log("hours difference in decimal: " + differenceHours)
        const hoursMinusLunch = parseFloat(differenceHours) - parseFloat(lunchTime)
        console.log("hours minus lunch in decimal: " + hoursMinusLunch)

        if (this.props.day === "Monday") {
            this.setState({mondayHours: hoursMinusLunch})
        }else if (this.props.day === "Tuesday") {
            this.setState({tuesdayHours: hoursMinusLunch})
        }else if (this.props.day === "Wednesday") {
            this.setState({wednesdayHours: hoursMinusLunch})
        }else if (this.props.day === "Thursday") {
            this.setState({thursdayHours: hoursMinusLunch})
        }

        //call parent func to update day totals under buttons
        this.props.updateHours(this.props.day, hoursMinusLunch)
    }

    handleTimeAdded(e) {

        let amountTimeToAdd = 0
        if (e.target.value === "Add 1/4 hour") amountTimeToAdd = 0.25 
        if (e.target.value === "Add 1/2 hour") amountTimeToAdd = 0.5 
        if (e.target.value === "Add 1 hour") amountTimeToAdd = 1.0 

        let reset = false
        if (e.target.value === "Reset") reset = true

        console.log("adding hours to day: " + e.target.id)
        if (e.target.id === "Monday") {
            let newHours
            reset ? newHours = 0 : newHours = this.state.mondayHours + amountTimeToAdd
            this.setState({mondayHours: newHours})
            //call parent func to update day totals under buttons
            this.props.updateHours(e.target.id, newHours)
        }
        if (e.target.id === "Tuesday") {
            let newHours
            reset ? newHours = 0 : newHours = this.state.tuesdayHours + amountTimeToAdd
            this.setState({tuesdayHours: newHours})
            this.props.updateHours(e.target.id, newHours)
        }
        if (e.target.id === "Wednesday") {
            let newHours
            reset ? newHours = 0 : newHours = this.state.wednesdayHours + amountTimeToAdd
            this.setState({wednesdayHours: newHours})
            this.props.updateHours(e.target.id, newHours)
        }
        if (e.target.id === "Thursday") {
            let newHours
            reset ? newHours = 0 : newHours = this.state.thursdayHours + amountTimeToAdd
            this.setState({thursdayHours: newHours})
            this.props.updateHours(e.target.id, newHours)
        }
    }

    render() {

        let displayHours

        if (this.props.day === "Monday") displayHours = this.state.mondayHours
        if (this.props.day === "Tuesday") displayHours = this.state.tuesdayHours
        if (this.props.day === "Wednesday") displayHours = this.state.wednesdayHours
        if (this.props.day === "Thursday") displayHours = this.state.thursdayHours

        return (
            <div>
                <h1>{this.props.day}</h1>
                <p>Hours: {ConvertToTime(displayHours)}</p>
                <p><button id={this.props.day} value="Add 1 hour" onClick={this.handleTimeAdded}>Add 1 hour</button></p>
                <p><button id={this.props.day} value="Add 1/2 hour" onClick={this.handleTimeAdded}>Add 1/2 hour</button></p>
                <p><button id={this.props.day} value="Add 1/4 hour" onClick={this.handleTimeAdded}>Add 1/4 hour</button></p>
                <p><button id={this.props.day} value="Reset" onClick={this.handleTimeAdded}>Reset</button></p>
                <p>Or input times:</p>
                <form onSubmit={this.handleHoursSubmitted}>
                <label labelfor="startTime">Start: </label>
                <input type="time" id="startTime" name="startTime" min="06:00" max="12:00" step="900" required />
                <label labelfor="endTime"> Finish: </label>
                <input type="time" id="endTime" name="endTime" min="12:00" max="20:00" step="900" required />
                <label labelfor="lunchTime"> Lunch: </label>
                <input type="time" id="lunchTime" name="lunchTime" min="00:00" max="05:00" step="900" required />
                <br />
                <br />
                <br />
                <input type="submit" id="submit" value="Submit" />
                </form>
                <br />
            </div>
        )
    }
}

Days.propTypes = {
    day: PropTypes.string.isRequired,
    updateHours: PropTypes.func.isRequired,
    initialMondayHours: PropTypes.number.isRequired,
    initialTuesdayHours: PropTypes.number.isRequired,
    initialWednesdayHours: PropTypes.number.isRequired,
    initialThursdayHours: PropTypes.number.isRequired
}

export default Days