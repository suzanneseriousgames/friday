import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {ConvertToTime} from './Helper'

class Friday extends Component {

    constructor(props) {
        super(props)
        this.state = {
            //hoursToWork: 0
            goHomeAt: 0,
            startTime: 0,
            lunchHours: 0
          }
          this.startTimeChanged = this.startTimeChanged.bind(this)
          this.lunchHoursChanged = this.lunchHoursChanged.bind(this)
          this.calcHoursLeft = this.calcHoursLeft.bind(this)
    }

    calcHoursLeft(start ,lunch) {
        
        let useThisStartTime
        let useThisLunchHours

        start ? useThisStartTime = start : useThisStartTime = this.state.startTime
        lunch ? useThisLunchHours = lunch : useThisLunchHours = this.state.lunchHours

        console.log("hours to work: " + this.props.hoursToWork)
        console.log("start time: " + useThisStartTime)
        console.log("lunch hours: " + useThisLunchHours)

        let homeAt = 0
        homeAt = Number(useThisStartTime) + Number(this.props.hoursToWork) + Number(useThisLunchHours)
        console.log("updating home at: " + homeAt)

        //convert decimal to time
        const homeAtTime = ConvertToTime(homeAt)

        this.setState({goHomeAt: homeAtTime})
    }

    startTimeChanged(e) {
        console.log("start time changed")
        this.setState({startTime: e.target.value})
        this.calcHoursLeft(e.target.value, null)
    }

    lunchHoursChanged(e) {
        console.log("lunch hours changed")
        this.setState({lunchHours: e.target.value})
        this.calcHoursLeft(null, e.target.value)
    }

    render() {

        let displayStartAndLunch = false
        if (this.props.hoursToWork < 12) displayStartAndLunch = true

        const maxStartTime = 24 - this.props.hoursToWork//can't start so late it will run into tomorrow
        const maxLunchHours = 5//love a long lunch me, but more than 5 hours is taking the mick

        return(
            <div>
                <h1>Friday!!!</h1>
                <p>This is how many more hours you need to work this week: {ConvertToTime(this.props.hoursToWork)}</p>
        {displayStartAndLunch ? <div><p>What time will you start? (24 hours decimal) </p><input type="number" placeholder={this.state.startTime} onChange={this.startTimeChanged} max={maxStartTime} /><p>How long for lunch?</p><input type="number" placeholder={this.state.lunchHours} onChange={this.lunchHoursChanged} max={maxLunchHours} /><h2>You can go home at: {this.state.goHomeAt}</h2></div> : null}
            </div>
        )
    }
}

Friday.propTypes = {
    hoursToWork: PropTypes.number.isRequired//hours left from Buttons
}

export default Friday