export function ConvertToTime(decimalHours) {
    decimalHours = Number(decimalHours)
    let timeHours
    const wholeHours = Math.floor(decimalHours)
    console.log("whole hours: " + wholeHours)
    const partHours = decimalHours - wholeHours
    console.log("part hours: " + partHours)
    const partHoursInMinutes = partHours * 60
    console.log("part hours in minutes: " + partHoursInMinutes)
    timeHours = wholeHours + ":" + partHoursInMinutes
    console.log("time in hours: " + timeHours)
    return timeHours 
}