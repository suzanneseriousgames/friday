import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {ConvertToTime} from './Helper'

class HoursToGo extends Component {
    //displays how many hours remaining

    constructor(props) {
        super(props)
        this.state = {
            hoursToGo: this.props.hours
        }
    }

    render() {
        return(
            <h2>Hours to go: {ConvertToTime(this.props.hours)}</h2>
        )
    }
}

HoursToGo.propTypes = {
    hours: PropTypes.number.isRequired
}

export default HoursToGo